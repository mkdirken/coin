/*
 Navicat Premium Data Transfer

 Source Server         : MYSQLSERVER
 Source Server Type    : MySQL
 Source Server Version : 100129
 Source Host           : 127.0.0.1:3306
 Source Schema         : skycoin

 Target Server Type    : MySQL
 Target Server Version : 100129
 File Encoding         : 65001

 Date: 18/11/2020 16:19:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for uye_cuzdan
-- ----------------------------
DROP TABLE IF EXISTS `uye_cuzdan`;
CREATE TABLE `uye_cuzdan`  (
  `uye_cuzdan_id` int NOT NULL AUTO_INCREMENT,
  `uye_id` int NULL DEFAULT NULL,
  `cuzdan_btc` decimal(13, 8) NULL DEFAULT NULL,
  `cuzdan_eth` decimal(13, 8) NULL DEFAULT NULL,
  `cuzdan_xrp` decimal(13, 8) NULL DEFAULT NULL,
  `cuzdan_ltc` decimal(13, 8) NULL DEFAULT NULL,
  PRIMARY KEY (`uye_cuzdan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_turkish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of uye_cuzdan
-- ----------------------------
INSERT INTO `uye_cuzdan` VALUES (1, 1, 2.00999682, 37.89918699, 61198.09195402, 249.66256026);

-- ----------------------------
-- Table structure for uyeler
-- ----------------------------
DROP TABLE IF EXISTS `uyeler`;
CREATE TABLE `uyeler`  (
  `uye_id` int NOT NULL AUTO_INCREMENT,
  `uye_adi` varchar(60) CHARACTER SET utf8 COLLATE utf8_turkish_ci NULL DEFAULT NULL,
  `uye_soyadi` varchar(40) CHARACTER SET utf8 COLLATE utf8_turkish_ci NULL DEFAULT NULL,
  `uye_sifresi` varchar(128) CHARACTER SET utf8 COLLATE utf8_turkish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`uye_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_turkish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of uyeler
-- ----------------------------
INSERT INTO `uyeler` VALUES (1, 'Mustafa Kemal', 'Dirken', NULL);

SET FOREIGN_KEY_CHECKS = 1;
