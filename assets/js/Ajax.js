


function GET_COIN_INFO(){ //ANLIK OLARAK 3 ADET CRYPTO PARANIN DEĞERLERİ BU FONKSİYON İLE ÇEKİLİYOR VE VİEW'DA TANIMLI ID'LERE GÖNDERİLİYOR
    $.ajax({
        url:$('body').attr('data-apiurl'),
        type:"GET",
        data:{'AJAX_ISLEM':'GET_COIN_INFO'},
        dataType:"json",
        success:function(data)
        {
            if(data.status=="success")
            {
                var COIN=jQuery.parseJSON(data.DATA).data;
                var BTC=COIN[0].last;
                var ETH=COIN[2].last;
                var LTC=COIN[4].last;
                var XRP=COIN[3].last;
                $('#canli_btc').html(BTC+" TL");
                $('#canli_eth').html(ETH+" TL");
                $('#canli_ltc').html(LTC+" TL");
                $('#canli_xrp').html(XRP+" TL");
            }
            else{
                alert("API ERİŞİMİ SORUNU");
            }
        },
        error:function () {
            alert("AJAX SORUNU MEVCUT");
        }
    });

}


function CUZDAN_DONUSTUR(){

    $.ajax({
        url:($('body').attr('data-baseurl')+'/Cuzdan').replace(/.*\//, ''),
        type:"POST",
        dataType:"json",
        data:$("#DONUSTURME_FORMU").serialize(),
        success:function(data)
        {
            if(data.status=="success")
            {
                var _CUZDAN=data.CUZDAN;
                var BTC=_CUZDAN["cuzdan_btc"];
                var ETH=_CUZDAN["cuzdan_eth"];
                var LTC=_CUZDAN["cuzdan_ltc"];
                var XRP=_CUZDAN["cuzdan_xrp"];
                $('#btc_cuzdan').html(BTC);
                $('#eth_cuzdan').html(ETH);
                $('#ltc_cuzdan').html(LTC);
                $('#xrp_cuzdan').html(XRP);
                alert(data.mesaj);
            }
            else{
                alert(data.mesaj);
            }
        },
        error:function () {
            alert("AJAX SORUNU MEVCUT");
        }
    });


}
