<?php
require_once(CORE . "View.php");
require_once(CORE . "Controller.php");
require_once(HELPER . "Fonksiyonlar.php");
class Application {

    protected $controller = VARSAYILAN_CONTROLLER; // VARSAYILAN CONTROLLER
    protected $action = "Index"; // VARSAYILAN CALISAN ISLEM FONKSIYONU
    protected $parameters = array();

    public function __construct(){
        $this->ParseURL();
        // Eğer Controller dosyası varsa dosyayı dahil ediyorum
        if(file_exists(CONTROLLER.$this->controller.".php")){
            require_once (CONTROLLER.$this->controller.".php");
            // Dahil edilen Controller sınıfından yeni bir Controller oluşturuyorum
            $this->controller = new $this->controller;
            // Oluşturulan Controller içerisinde Action varsa Action çağır.
            if(method_exists($this->controller, $this->action)){
                call_user_func_array([$this->controller, $this->action], $this->parameters);
                // call_user_func_array PHP fonksiyonu ile controller içerisindeki action(fonsyionu) çalışıyorum
                // ayrıca paremetre varsa bu parametreleri fonksiyona gönderiyorum
            } else {
                $view = new View(HATA_VIEW.'404');
                return $view->Olustur();
            }
        } else {
            $view = new View(HATA_VIEW.'404');
            return $view->Olustur();
        }

    }



    /**
     * ParseURL methodu genel mantığı ile şu işlemleri yapar;
     *
     * $_SERVER["REQUEST_URI"] yardımı ile istemci tarafından gönderilen URL yakalanır.
     *
     * trim() fonkisyonu ile URL sonunda bulunursa "/" karakteri temizlenir.
     *
     * explode() fonksiyonu ile URL "/" karakterine göre dizileştirilir.
     *
     * $url değişkeni bir dizi olur. [0] => Controller Adı, [1] => Action Adı, [2} ve Sonrası => Parametreler
     *
     * unset() fonksiyonu ile $url değişkeninde varsa [0] ve [1] indis numaralı elemenlar temizlenir.
     * Geriye kalan değerler parametrelerdir.
     */
    protected function ParseURL(){
        // request işlemini doğru alabilmek için local yolu varsa bulup kaldırıyorum
        $dirname=dirname($_SERVER['SCRIPT_NAME']);
        $LOCAL_YOLU=str_replace($dirname,null,$_SERVER["REQUEST_URI"]);
        $request = trim($LOCAL_YOLU,'/');

        if (!empty($request)){
            // URL ICERISINDEKI GET METODLARINI TEMIZLEYETLIM
            $url = strtok($request, '?');
            $url = explode("/", $url);
            $this->controller = isset($url[0]) ? $url[0] : $this->controller;
            $this->action = isset($url[1]) ? $url[1] :  $this->action;
            unset($url[0], $url[1]);
            $this->parameters = !empty($url) ? array_values($url) : $this->parameters;
        }
    }
}
?>

