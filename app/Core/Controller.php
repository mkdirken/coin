<?php
// CONTROLLER KLASORUNDEKI SINIFLARA GENEL NESNE KALITIMI  OLUSTURACAGIM
class Controller {
    protected $view;

    public function View($view_dosya_adi, $model = []){
        $this->view = new View($view_dosya_adi, $model);
        return $this->view->Olustur();
    }

    public function Redirect($path)
    {
        header("Location: {$path}");
    }
}
?>