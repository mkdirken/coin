<?php
// TASARIMLARIN GOSTERIP ICERISINE VERI GONDEREBILECEGIMIZ VIEW SINIFI
class View {
    protected $view_dosyasi;
    protected $view_icerigi;

    public function __construct($view_dosyasi, $view_icerigi=array()){
        // SINIFA GELEN VERILERI ICERIDE KULLANABILMEK ICIN VERILERI SINIFA GLOBAL OLARAK ATIYORUM
        $this->view_dosyasi = $view_dosyasi;
        $this->view_icerigi = $view_icerigi;
    }

    public function Olustur(){
        // VIEW DOSYASI MEVCUTSA CAGIRMA ISLEMI YAPICAZ
        if(file_exists(VIEW.$this->view_dosyasi.".php")){
            extract($this->view_icerigi); // VIEWE GELEN ARRAY BICIMDEKI VERIYI PARCA HALINDEKI DEGISKENE ATIYORUM
            ob_start();
            ob_get_clean();
            include_once (VIEW.$this->view_dosyasi.".php");
        }
    }
}
?>