<?php
define("HOST", VERITABANI_HOSTNAME);
define("DBNAME", VERITABANI_ADI);
define("UNAME", VERITABANI_KULLANICI);
define("PASSWD", VERITABANI_SIFRESI);
define("CHARSET", "utf8");

class Model extends PDO
{

    public function __construct()
    {
        try {
            parent::__construct("mysql:host=" . HOST . ";dbname=" . DBNAME, UNAME, PASSWD);
            $this->query('SET CHARACTER SET ' . CHARSET);
            $this->query('SET NAMES ' . CHARSET);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (PDOException $error) {
            $error->getMessage();
        }
    }

    public function _result($tablo_adi){
        return $this->query("SELECT * FROM $tablo_adi", PDO::FETCH_ASSOC);
    }





}
?>

