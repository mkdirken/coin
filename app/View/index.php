<?php include(VIEW.'BASE/header.php'); ?>



    <h3 class="text-left">CANLI COİN BORSASI (₺)</h3>



    <div class="row">


        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">BTC</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="canli_btc">0</div>
                        </div>
                        <div class="col-auto">
                            <i class="fab fa-bitcoin fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">ETH</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="canli_eth">0</div>
                        </div>
                        <div class="col-auto">
                            <i class="fab fa-ethereum fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">LTC</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="canli_ltc">0</div>
                        </div>
                        <div class="col-auto">
                            <i class="fab fa-turkish-lira fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">XRP</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800" id="canli_xrp">0</div>
                        </div>
                        <div class="col-auto">
                            <i class="fab fa-turkish-lira fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>




<h3 class="text-left">CÜZDAN (Adet)</h3>
<div class="row">


    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">BTC</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800" id="btc_cuzdan"><?php echo $CUZDAN['cuzdan_btc'];?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fab fa-btc fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">ETC</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800" id="eth_cuzdan"><?php echo $CUZDAN['cuzdan_eth'];?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fab fa-ethereum fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">LTC</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800" id="ltc_cuzdan"><?php echo $CUZDAN['cuzdan_ltc'];?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-archive fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">XRP</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800" id="xrp_cuzdan"><?php echo $CUZDAN['cuzdan_xrp'];?></div>
                    </div>
                    <div class="col-auto">
                        <i class="fa fa-archive fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<h3 class="text-center">DÖNÜŞTÜRME</h3>
<div class="row col-12 text-center justify-content-center">
    <form action="#" method="post" id="DONUSTURME_FORMU" onsubmit="CUZDAN_DONUSTUR();return false;">
        <input type="hidden" name="AJAX_ISLEM" value="CUZDAN_DONUSTUR">
        <div class="row">
            <div class="col-4">
                <label>Dönüştürülecek</label>
                <select class="form-control" name="donusturulecek">
                    <option value="BTC_TRY">BTC</option>
                    <option value="ETHTRY">ETC</option>
                    <option value="LTCTRY">LTC</option>
                    <option value="XRPTRY">XRP</option>
                </select>
            </div>
            <div class="col-4">
                <label>Miktar/Adet</label>
                <input type="number" class="form-control" placeholder="Miktar" name="miktar" required>
            </div>
            <div class="col-4">
                <label>Dönüşen</label>
                <select class="form-control" name="donusen">
                    <option value="BTC_TRY">BTC</option>
                    <option value="ETHTRY">ETC</option>
                    <option value="LTCTRY">LTC</option>
                    <option value="XRPTRY">XRP</option>
                </select>
            </div>
        </div>
        <div class="form-group mt-2">
            <button class="btn btn-success btn-icon-split" type="submit">
                <span class="icon text-white-50">
                    <i class="fas fa-random"></i>
                </span>
                <span class="text">DÖNÜŞTÜR</span>
            </button>
        </div>
    </form>
</div>




<?php include(VIEW.'BASE/footer.php'); ?>

<script>
    $(function (){
        GET_COIN_INFO();
        setInterval(function(){
            GET_COIN_INFO();
        }, 3*1000);
    });
</script>
