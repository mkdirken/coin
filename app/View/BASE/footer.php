
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->


<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; SKYNEB - COIN 2020</span>
        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Page Wrapper -->

<script src="<?php echo base_url('/assets/vendor/jquery/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/sb-admin-2.min.js'); ?>"></script>
<script src="<?php echo base_url('/assets/js/Ajax.js'); ?>"></script>

</body>

</html>