<?php

// SISTEM ICIN GLOBAL DEGISKENLERI OLUSTURUYORUM

define('ORTAM','development');
define('BASE_URL','http://localhost/skycoin/');
define('VARSAYILAN_CONTROLLER','Anasayfa');

define('VERITABANI_HOSTNAME','localhost');
define('VERITABANI_ADI','skycoin');
define('VERITABANI_KULLANICI','root');
define('VERITABANI_SIFRESI','');


// PROJE ICERISINDE APP İÇERİSİNDEKİ KLASÖRLERE VE TEMEL KLASÖRLERE KOLAY ERİŞİM İÇİN SABIT DEGİŞKENE ATIYORUM
define("ROOT", __DIR__.DIRECTORY_SEPARATOR);
define("MODEL", ROOT."Model".DIRECTORY_SEPARATOR);
define("VIEW", ROOT."View".DIRECTORY_SEPARATOR);
define("CONTROLLER", ROOT."Controller".DIRECTORY_SEPARATOR);
define("CORE", ROOT."Core".DIRECTORY_SEPARATOR);
define("HELPER", ROOT."Helper".DIRECTORY_SEPARATOR);
define("HATA_VIEW","error".DIRECTORY_SEPARATOR);



?>