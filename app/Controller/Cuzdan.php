<?php
require_once(MODEL . "Uyemodel.php");


class Cuzdan extends Controller {

    protected $UYE_MODEL;

    function __construct()
    {
        $this->UYE_MODEL=new Uyemodel();
    }

    public function Index(){

        $AJAX_ISLEM=p('AJAX_ISLEM');
        if($AJAX_ISLEM=="CUZDAN_DONUSTUR"){
            require_once (CONTROLLER.'API.php');
            $API=new API();
            $donusturulecek=p('donusturulecek');
            $miktar=p('miktar');
            $donusen=p('donusen');
            if ($miktar>0 && $miktar!=""){
                if($donusturulecek!=$donusen){
                    $_DONUSTURULEN=$API->GET_SYMBOL_BTC($donusturulecek);
                    $_DONUSEN=$API->GET_SYMBOL_BTC($donusen);
                    $_DONUSTURULEN_FIYAT=$_DONUSTURULEN->data[0]->last;
                    $_DONUSEN_FIYAT=$_DONUSEN->data[0]->last;
                    $VERI_ARRAY=array(
                        'DONUSTURULEN_AD'=>$this->BTCTURK_PAIR_ADLARI_ILE_UYE_CUZDAN_KOLON_ADINI_BUL($_DONUSTURULEN->data[0]->pair),
                        'DONUSEN_AD'=>$this->BTCTURK_PAIR_ADLARI_ILE_UYE_CUZDAN_KOLON_ADINI_BUL($_DONUSEN->data[0]->pair),
                        'DONUSTURULEN_FIYAT'=>$_DONUSTURULEN_FIYAT,
                        'DONUSEN_FIYAT'=>$_DONUSEN_FIYAT,
                        'MIKTAR'=>$miktar,
                    );
                    $mesaj["status"]="success";
                    $mesaj["status"]="success";
                    $mesaj["SONUC"]=$this->UYE_MODEL->UYE_CUZDAN_DONUSTUR(1,$VERI_ARRAY);
                    $mesaj["CUZDAN"]=$this->UYE_MODEL->UYE_CUZDAN_CEK();
                    $mesaj["mesaj"]="Dönüştürme başarıyla tamamlandı";
                }else{
                    $mesaj["status"]="warning";
                    $mesaj["mesaj"]="Coin değerleri değişmedi";
                }
            }
            else{
                $mesaj["status"]="warning";
                $mesaj["mesaj"]="Miktar Değeri Boş Bırakılamaz";
            }

            echo json_encode($mesaj);

        }

    }


    private function BTCTURK_PAIR_ADLARI_ILE_UYE_CUZDAN_KOLON_ADINI_BUL($AD){
        switch ($AD){
            case "BTCTRY" :
                return "cuzdan_btc";
            case "ETHTRY":
                return "cuzdan_eth";
            case "LTCTRY":
                return "cuzdan_ltc";
            case "XRPTRY":
                return "cuzdan_xrp";
            default:
                return $AD;
        }
    }

}
?>