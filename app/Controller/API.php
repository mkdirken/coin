<?php


class API extends Controller {



    function __construct()
    {

    }
    public function Index(){
        $AJAX_ISLEM=g('AJAX_ISLEM');
        if($AJAX_ISLEM=="GET_COIN_INFO"){
            //BTCTURK APISINDEN VERILER ALINACAK
            $base = "https://api.btcturk.com";
            $method = "/api/v2/ticker";
            $uri = $base.$method;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $uri);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, "CURL_HTTP_VERSION_1_2");
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            echo json_encode(array('status'=>'success','DATA'=>$result));

        }else{
            echo json_encode(array());
        }

    }

    function GET_SYMBOL_BTC($SYMBOL){
        $base = "https://api.btcturk.com";
        $method = "/api/v2/ticker?pairSymbol=".$SYMBOL;
        $uri = $base.$method;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, "CURL_HTTP_VERSION_1_2");
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            //print_r(curl_error($ch));
        }
        return json_decode($result);
    }

}
?>