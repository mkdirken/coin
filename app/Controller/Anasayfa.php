<?php
require_once(MODEL . "Uyemodel.php");
class Anasayfa extends Controller {

    protected $UYE_MODEL;

    function __construct()
    {
        $this->UYE_MODEL=new Uyemodel();
    }

    public function Index(){

        $UYE=$this->UYE_MODEL->UYE_CEK(1);
        $UYE_CUZDANI=$this->UYE_MODEL->UYE_CUZDAN_CEK(1);

        $this->View('index',array(
            'UYE'=>$UYE,
            'CUZDAN'=>$UYE_CUZDANI
        ));

    }

}
?>