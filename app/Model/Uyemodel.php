<?php
require_once(CORE . "Model.php");
class Uyemodel extends Model
{

    function __construct()
    {
        parent::__construct();
    }


    public function UYE_CEK($UYE_ID=1){
        $query = $this->query("SELECT * FROM uyeler WHERE uye_id = '{$UYE_ID}'")->fetch(PDO::FETCH_ASSOC);
        return $query;
    }

    public function UYE_CUZDAN_CEK($UYE_ID=1){
        $query = $this->query("SELECT * FROM uye_cuzdan WHERE uye_id = '{$UYE_ID}'")->fetch(PDO::FETCH_ASSOC);
        return $query;
    }

    public function UYE_CUZDAN_DONUSTUR($UYE_ID=1,$VERI=array()){
        $CUZDAN=$this->UYE_CUZDAN_CEK($UYE_ID); // UYE CUZDANINI ÇEKİYORUM
        $SONUC=($VERI['DONUSTURULEN_FIYAT']/$VERI['DONUSEN_FIYAT'])*$VERI['MIKTAR'];
        $DONUSTURULEN_FIYAT=$CUZDAN[$VERI['DONUSTURULEN_AD']];
        $DONUSEN_FIYAT=$CUZDAN[$VERI['DONUSEN_AD']];
        $DONUSTURULEN_FIYAT=$DONUSTURULEN_FIYAT-$VERI['MIKTAR'];
        $DONUSEN_FIYAT=$DONUSEN_FIYAT+ $SONUC;
        $query = $this->prepare("UPDATE uye_cuzdan SET ".$VERI['DONUSTURULEN_AD']." = :donusturulen , ".$VERI['DONUSEN_AD']." = :donusen   WHERE uye_id = :uye_id");
        $update = $query->execute(array(
            "donusturulen" => $DONUSTURULEN_FIYAT,
            "donusen" => $DONUSEN_FIYAT,
            "uye_id" => $UYE_ID
        ));
        return $update;

    }


}
?>