<?php
/**
 * Erişim kök dizindeki index.php ten geliyorsa
 * Config(ayar) dosyamızı içerisinde GLOBAL değişkenlerde mevcut çağırıyorum
 * Çekirdek Uygulama Dosyası(CORE/Application)nı dahil edip çalıştırıyorum
 */
if(KOK_INDEX===TRUE){
    require_once("config.php");
    require_once(CORE . "Application.php");
    new Application();
}else{
    die('<h1 align="text-center">Erişim İzniniz Yok</h1>');
}
?>
